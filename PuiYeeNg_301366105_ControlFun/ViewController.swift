//
//  ViewController.swift
//  PuiYeeNg_301366105_ControlFun
//  Author: Pui Yee Ng
//  StudentID: 301366105
//  Date: 9/19/2023
//  App description: Control Fun App
//  Version: First Version


import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameInput: UITextField!
    
    @IBOutlet weak var numberInput: UITextField!
    
    @IBOutlet weak var myLabel: UILabel!
    
    @IBOutlet weak var leftSwitch: UISwitch!
    
    @IBOutlet weak var rightSwitch: UISwitch!
    
    
    @IBOutlet weak var doSomethingButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func onSliderChanged(_ sender: UISlider) {
        myLabel.text = "\(lroundf(sender.value))"
    }
    
    
    @IBAction func onLeftSwitchChanged(_ sender: UISwitch) {
        
        let setting = sender.isOn
        leftSwitch.setOn(setting, animated: true)
        rightSwitch.setOn(setting, animated: true)
    }
    
    @IBAction func onRightSwitchChanged(_ sender: UISwitch) {
        
        let setting = sender.isOn
        leftSwitch.setOn(setting, animated: true)
        rightSwitch.setOn(setting, animated: true)
        
    }
    
    @IBAction func toggleControls(_ sender: UISegmentedControl) {
            if sender.selectedSegmentIndex == 0 {  // "Switches" is selected
                leftSwitch.isHidden = false
                rightSwitch.isHidden = false
                doSomethingButton.isHidden = true
            } else {
                leftSwitch.isHidden = true
                rightSwitch.isHidden = true
                doSomethingButton.isHidden = false
            }
     }
    
    @IBAction func onButtonPressed(_ sender: UIButton) {
            let controller = UIAlertController(title: "Are You Sure?",
                message:nil, preferredStyle: .actionSheet)
            
            let yesAction = UIAlertAction(title: "Yes, I'm sure!",
                style: .destructive, handler: { action in
                let msg = self.nameInput.text!.isEmpty
                    ? "You can breathe easy, everything went OK."
                    : "You can breathe easy, "
                    + self.nameInput.text!
                    + ", everything went OK."
                let controller2 = UIAlertController(
                    title:"Something Was Done",
                    message: msg, preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Phew!", style: .cancel, handler: nil)
                controller2.addAction(cancelAction)
                self.present(controller2, animated: true,completion: nil)
            })
            
            let noAction = UIAlertAction(title: "No way!", style: .cancel, handler: nil)
            
            controller.addAction(yesAction)
            controller.addAction(noAction)
            
            if let ppc = controller.popoverPresentationController {
                ppc.sourceView = sender
                ppc.sourceRect = sender.bounds
                ppc.permittedArrowDirections = .down
            }
            
            present(controller, animated: true, completion: nil)
    }
    
}

